var express = require('express'),
    app = express(),
    serveStatic = require('serve-static'),
    path = require('path'),
    port = 8000,
    target = './app/dist';


app.use(express.static(path.join(__dirname, target)));

app.use(serveStatic(path.join(__dirname, target), {
    'index': ['uikit.html']
}));



app.listen(port);
console.log('Listening on port ' + port + '\nTarget: ' + target);
