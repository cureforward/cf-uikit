#How to generate icons:

## For Mac users:
* npm install svgexport -g
* brew install imagemagick

To convert **SVG** to **PNG** you just need to run this ```svgexport favicon.svg favicon-XXX.png XXX:```

For app icons use **faicon_app.svg**:
```
svgexport favicon_app.svg favicon-57.png 57:
svgexport favicon_app.svg favicon-57.png 57:
svgexport favicon_app.svg favicon-72.png 72:
svgexport favicon_app.svg favicon-114.png 114:
svgexport favicon_app.svg favicon-120.png 120:
svgexport favicon_app.svg favicon-144.png 144:
svgexport favicon_app.svg favicon-152.png 152:
svgexport favicon_app.svg favicon-144.png 144:
```

For browsers:
```
svgexport favicon.svg favicon-16.png 16:
svgexport favicon.svg favicon-24.png 24:
svgexport favicon.svg favicon-32.png 32:
svgexport favicon.svg favicon-48.png 48:
svgexport favicon.svg favicon-64.png 64:
```

*XXX* - size what you need.

After you get all pngs you need to compound these files:
* favicon-16.png
* favicon-24.png
* favicon-32.png
* favicon-48.png
* favicon-64.png

into ```favicon.ico``` and __place it into the root folder__

The command itself:

```convert favicon-16.png favicon-24.png favicon-32.png favicon-48.png favicon-64.png favicon.ico```

#Sources
* https://scotch.io/tutorials/all-favicons-for-all-devices-and-sizes
* https://github.com/shakiba/svgexport