var gulp = require('gulp'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    srcPath = './app/src',
    destPath = './app/dist';

gulp.task('watch', function() {
    gulp.watch(srcPath + '/img/**/*', ['copyImg']);
    gulp.watch(srcPath + '/styles/**/*.less', ['genCss']);
    gulp.watch(srcPath + '/uikit.tpl.html', ['genHtml']);
});

gulp.task('genCss', function() {
    return gulp.src(srcPath + '/styles/uikit/uikit.less')
        .pipe(less().on('error', function(err) {
            console.log(err);
        }))
        .pipe(cssmin().on('error', function(err) {
            console.log(err);
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(destPath + '/css'));

});

gulp.task('genHtml', function() {
    return gulp.src(srcPath + '/uikit.tpl.html')
        .pipe(rename('uikit.html'))
        .pipe(gulp.dest(destPath));
});

gulp.task('copyFonts', function() {
    return gulp.src(srcPath + '/fonts/**/*', {
        read: true
    }).pipe(gulp.dest(destPath + '/fonts'));
});

gulp.task('copyImg', function() {
    return gulp.src(srcPath + '/img/**/*', {
        read: true
    }).pipe(gulp.dest(destPath + '/img'));
});

gulp.task('default', ['genCss', 'genHtml', 'copyFonts', 'copyImg', 'watch']);